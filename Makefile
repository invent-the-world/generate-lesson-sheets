# From [makefile for xelatex document](https://gist.github.com/xinhaoyuan/e986aca1332ab6b345b854bf57300643)
.PHONY: all clean

-include dep.mk

OUT ?= out

dep.mk:
	@latexmk -silent -xelatex -dependents lesson-sheet.temp -MF dep.mk -outdir=${OUT}

clean:
	@latexmk -silent -C -outdir=${OUT}
	@rm dep.mk

out/main.pdf:
	latexmk -xelatex lesson-sheet.temp -outdir=out

all: out/main.pdf
