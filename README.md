# Generate ITW lesson sheets #

## Requirements ##

### System dependencies ###

#### Arch Linux
- `texlive-most`
- `ttf-ubuntu-font-family`
- `ttf-roboto`

### Python dependencies ###

- PyYAML
- Jinja2
- pygments


## Installation ##

There are two projects that you'll need to clone:

- [invent-the-world / itw-worksheets ·
  GitLab](https://odo.stooj.org/invent-the-world/itw-worksheets) contains the
  lessons
- [invent-the-world / generate-lesson-sheets ·
  GitLab](https://odo.stooj.org/invent-the-world/generate-lesson-sheets)
  contains the generation scripts, turning the lessons into printable sheets.

## Usage ##

To generate a sheet, `cd` into the "generate-lesson-sheets" directory, switch to
the appropriate `virtualenv` if you are using one and run:

```bash
python3 generate-lesson.py /path/to/itw/worksheet.yaml
```

PDF files are generated in the source directory, named after the sheet title and
number.

Sometimes you just gotta catch 'em all, and you can generate ALL THE SHEETS
using a loop or a find command:

```bash
find /path/to/itw-worksheets/ -name *.yaml -exec python generate-lesson.py '{}' \;
```

## Tidying up ##

Because of the way this works at the moment, sometimes things can be left a bit
messy after a crash.

Run the following to tidy up the directory:

```bash
git clean -i
```

## References ##

- [LaTeX templates with Python and Jinja2 to generate PDFs | Brad Erickson](http://eosrei.net/articles/2015/11/latex-templates-python-and-jinja2-generate-pdfs)
