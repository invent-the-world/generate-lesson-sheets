#!/usr/bin/env python3
# vim:fileencoding=utf-8

import glob
import jinja2
import os
import shutil
import subprocess
import sys

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import LatexFormatter
from yaml import load as yaml_load

DEFAULT_DO_ICON = 'mouse.png'
DEFAULT_RUN_ICON = 'terminal.png'
DEFAULT_BONUS_ICON = 'chest.png'
DEFAULT_COPY_ICON = 'geany.png'

DEFAULT_ICONS = {
    'do': DEFAULT_DO_ICON,
    'run': DEFAULT_RUN_ICON,
    'bonus': DEFAULT_BONUS_ICON,
    'copy': DEFAULT_COPY_ICON,
}

SBOX_TEMPLATE = 'templates/sbox.tex.jinja'
MBOX_TEMPLATE = 'templates/mbox.tex.jinja'
THREEPARTBOX_TEMPLATE = 'templates/threepartbox.tex.jinja'
SMBOX_TEMPLATE = 'templates/smbox.tex.jinja'
DEFEATED_TEMPLATE = 'templates/defeated.tex.jinja'
GATEKEEPER_TEMPLATE = 'templates/gatekeeper.tex.jinja'

TEMP_TEX_FILENAME = 'lesson-sheet.temp'  # Remember to match this in Makefile!


latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%>',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)

template = latex_jinja_env.get_template('lesson-template.tex')


def get_box_type(task):
    if 'success' in task:
        if 'error' in task:
            return 'Threepartbox'
        else:
            # TODO Figure out whether to use an SMBox or MMBox
            return 'SMbox'
    elif 'result' in task:
        return 'SMbox'
    # - Mbox
    # TODO Figure out whether to use an SBox or MBox
    return 'SBox'


def parse_data_block(lesson):

    if not isinstance(lesson, dict):
        # Must be a flat block (no internal keys)
        return lesson

    block_type = lesson.get('type', 'text')

    if block_type == 'code':
        # TODO: Pass in default language
        default_language = 'python3'
        language = lesson.get('language', default_language)
        lexer = get_lexer_by_name(language, stripall=True)
        formatter = LatexFormatter()

        data = highlight(lesson['data'], lexer, formatter)
    else:
        data = lesson['data']

    return data


def generate_gatekeeper_block(lesson):
    template = latex_jinja_env.get_template(GATEKEEPER_TEMPLATE)
    content = template.render(
        image=parse_data_block(lesson['image'])
    )
    if lesson.get('half-width', False):
        content = r"\begin{halfpagebox}" + \
                "\n" + content + "\n" + \
                r"\end{halfpagebox}"

    return content


def create_block(block_type, lesson):
    if block_type == 'defeated':
        template = latex_jinja_env.get_template(DEFEATED_TEMPLATE)
        content = template.render(
            body=parse_data_block(lesson.get('task', "Gatekeeper Defeated!"))
        )

        return content

    if block_type == 'gatekeeper':
        return generate_gatekeeper_block(lesson)

    icon = lesson.get('icon', DEFAULT_ICONS.get(block_type))
    box_type = get_box_type(lesson)

    if box_type == 'SBox':
        template = latex_jinja_env.get_template(SBOX_TEMPLATE)
        content = template.render(
            block_type=block_type,
            icon=icon,
            body=parse_data_block(lesson['task']),
        )
    elif box_type == 'Threepartbox':
        template = latex_jinja_env.get_template(THREEPARTBOX_TEMPLATE)
        content = template.render(
            block_type=block_type,
            icon=icon,
            upper_content=parse_data_block(lesson['task']),
            success_content=parse_data_block(lesson['success']),
            error_content=parse_data_block(lesson['error']),
        )
    elif box_type == 'SMbox':
        template = latex_jinja_env.get_template(SMBOX_TEMPLATE)
        content = template.render(
            block_type=block_type,
            icon=icon,
            upper_content=parse_data_block(lesson['task']),
            result_content=parse_data_block(lesson['result']),
        )

    else:
        print("Unknown box type: {}".format(box_type), file=sys.stderr)
        return None

    if lesson.get('pagebreak', False):
        content = '\startsecondpage\n' + content

    if lesson.get('half-width', False):
        # FIXME: Sbox doesn't work with half-width boxes, which is fair
        # enough. Need to regenerate the content to use an MBox for now
        template = latex_jinja_env.get_template(MBOX_TEMPLATE)
        content = template.render(
            block_type=block_type,
            icon=icon,
            body=parse_data_block(lesson['task']),
        )
        content = r"\begin{halfpagebox}" + \
                  "\n" + content + "\n" + \
                  r"\end{halfpagebox}"

    return content


def find_worksheet_root(source):
    flag_folder = ".git"
    path = os.path.abspath(os.path.dirname(source))
    folders = os.listdir(path)
    while(flag_folder not in folders):
        path = os.path.abspath(os.path.join(path, os.pardir))
        folders = os.listdir(path)

        if(path == '/'):
            print("Could not find the worksheet root folder that "
                  "contains {}".format(flag_folder), file=sys.stderr)
            sys.exit(1)
    return path


def find_image_folders(worksheet_root):

    image_folders = []

    for path, dirnames, _ in os.walk(worksheet_root):
        if("images" in dirnames):
            image_folders.append(os.path.join(path, 'images'))

    if len(image_folders) == 0:
        print("Couldn't find any image folder", file=sys.stderr)
        sys.exit(1)

    return image_folders


if len(sys.argv) < 2:
    print("Please provide source yaml file to read", file=sys.stderr)
    sys.exit(1)

source = sys.argv[1]

if not os.path.isfile(source):
    print("Couldn't read {}".format(source), file=sys.stderr)
    sys.exit(1)

# TODO - add image_dir as an optional cmdline argument
worksheet_root = find_worksheet_root(source)
image_folders = find_image_folders(worksheet_root)

# Do a thorough clean up
subprocess.call(['make', 'clean'], shell=True)
if os.path.isdir('out'):
    shutil.rmtree('out')
if os.path.isfile('dep.mk'):
    os.remove('dep.mk')

with open(source, 'r') as fh:
    payload = yaml_load(fh)

# Only copy files if they are missing from the figures directory
for image_dir in image_folders:
    files_to_copy = [
        os.path.basename(f) for f in glob.glob("{}/*".format(image_dir))
        if os.path.basename(f) not in map(
            os.path.basename, glob.glob("figures/*"))
    ]

    for f in files_to_copy:
        print("Copying '{}' to figures directory".format(f))
        shutil.copy(os.path.join(image_dir, f), "figures/")

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%>',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)

template = latex_jinja_env.get_template('lesson-template.tex')

# Yuck! FIXME
highlight_style_defs = LatexFormatter().get_style_defs()

title = None
default_language = None
course = None
page_icon = None

for sheet in payload:
    # Make sure title is set on the first item at least
    if 'title' in sheet:
        title = sheet['title']
    else:
        if title is None:
            print("Could not find the sheet title", file=sys.stderr)
            sys.exit(1)
    sheet_number = sheet.get('sheet-number', 1)

    if default_language is None:
        default_language = sheet.get('default-language', 'python3')
    else:
        default_language = sheet.get('default-language', default_language)

    if course is None:
        course = sheet.get('course', 'python')
    else:
        course = sheet.get('course', course)

    goal = sheet['goal']
    flavour_heading = sheet['flavour-heading']
    if page_icon is None:
        page_icon = sheet.get('page_icon', None)
    else:
        page_icon = sheet.get('page_icon', page_icon)

    homework = sheet.get('homework', False)

    body = []
    for lesson in sheet['lessons']:
        body.append(create_block(lesson['type'], lesson))

    if page_icon is not None:
        shutil.copy('figures/{}', 'figures/page_icon.png')
    else:
        shutil.copy('figures/blank.png', 'figures/page_icon.png')

    context = {
        'style_defs': highlight_style_defs,
        'title': title,
        'sheet_number': sheet_number,
        'task_description': goal,
        'flavour_subheading': flavour_heading,
        'body': body,
    }

    if homework:
        context['homework'] = homework

    with open(TEMP_TEX_FILENAME, 'w') as fh:
        fh.write(template.render(
            context
        ))

    subprocess.call(['make', 'all'])

    shutil.move(
        'out/lesson-sheet.pdf',
        '{}-{}.pdf'.format(sheet_number, title)
    )
    print("{}-{}.pdf created".format(sheet_number, title))

    os.remove(TEMP_TEX_FILENAME)

for f in files_to_copy:
    os.remove(os.path.join('figures', f))
