#!/usr/bin/env python3
# vim:fileencoding=utf-8

import glob
import os
import subprocess
import unittest
import yaml

WORKSHEET_DIRECTORY = os.path.join(
    os.path.expanduser('~'),
    'programming',
    'invent',
    'itw-worksheets')


@unittest.skipUnless(os.path.isdir(WORKSHEET_DIRECTORY),
                     "Couldn't find worksheet directory")
class TestYamlParsing(unittest.TestCase):

    def test_yaml_syntax(self):
        """Test that all yaml files can be read by parser."""
        yaml_files = glob.glob(
            os.path.join(WORKSHEET_DIRECTORY, '**', '*.yaml'), recursive=True)
        for src in yaml_files:
            try:
                yaml.load(src)
            except yaml.YAMLError as esc:
                if hasattr(exc, 'problem_mark'):
                    mark = exc.problem_mark
                    self.fail(
                        "Yaml parse error in file {}, line {}, col {}".format(
                            src, mark.line + 1, mark.column + 1)
                    )
                else:
                    self.fail(
                        "Yaml parse error in file {}".format(src))

    def test_pdf_generation(self):
        """Test that each file generates pdf document.

        WARNING: This deletes exising pdfs.

        """
        self.addCleanup(self.remove_pdfs)
        self.remove_pdfs()
        yaml_files = glob.glob(
            os.path.join(WORKSHEET_DIRECTORY, '**', '*.yaml'), recursive=True)
        for src in yaml_files:
            try:
                subprocess.call([
                    'python', 'generate-lesson.py', src],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    timeout=30
                )
            except subprocess.TimeoutExpired:
                self.fail("{} timed out".format(src))
            self.assertGreater(len(glob.glob('*.pdf')), 0,
                               "{} did not create pdf".format(src))
            self.remove_pdfs()

    def remove_pdfs(self):
        """Remove pdfs from working directory."""
        [os.remove(f) for f in glob.glob('*.pdf')]

if __name__ == '__main__':
    unittest.main()
