# Generate ITW lesson sheets #

## Compilation ##

This template requires `XeLaTeX` compilation to support the custom fonts. It
will require more than one compilation to ensure that all elements are placed
correctly.

## Page Styles ##

Two page styles are defined in this template:

- `firstpage`
- `secondpage`

The `firstpage` style is the default and will apply to content up until the
custom command `\startsecondpage`, which will insert a pagebreak and change the
pagestyle to be `secondpage`.

## Special Text Formatting ##

### Code formatting ###

#### Inline code ####

For inline code snippets, enclose the text within `|`. Please note that some
special characters may need to be escaped.

```
Print your name by adding |print(name)|
```

#### Code blocks ####

Blocks of code should be placed within the `lstlisting` environment.
Furthermore, code that should be coloured orange should be enclosed within `@`
symbols.

```latex
\begin{lstlisting}[style=base]
weapon1 = "sword"
weapon2 = "axe"
print((@"You have my "@ + weapon1 + @" and my "@ + weapon2)
\end{lstlisting}
```

### Emphasising text ###

To emphasise text, use the custom command `\emphasise{text to be emphed}`. This
will make the text bold and itw-purple.

### Monospace coloured text ###

To format text to be any predefined colour, use `\coloredtext{colour}{text to
format}`

```latex
\coloredtext{lime}{success:}
```

## Text box sigils ##

The sigils or icons in the top-right corner of text boxes should be `png` images
with a transparent background. These should be added to the assets directory on
owncloud. The sigils are automatically resized, so to fine tune the size and/or
placement of the new sigils, adjust the amount of whitespace (transparentspace)
surrounding the image when creating the png.

## Special icons/figures within text boxes ##

To place sigils or other special icons within the text, use the custom command
`\AddIcon{filename}{height}`.

```latex
\AddIcon{icons/console.png}{13pt}
```

This will place the `console.png` within the icons directory into the text and
constrain the height of the image to be 13pt.

The command can be used to place figures within text boxes. The following
example when placed within a text box will place the `consold.png` with a
constrained height of 2cm and will centre it within the text body:

```latex
\begin{center}
	\AddIcon{icons/console.png}{2cm}
\end{center}
```

## Text boxes ##

The custom text boxes are formatted as custom environments. As such, they are
enclosed by `\begin{custombox1}` and `\end{custombox1}`.

```latex
\begin{Sbox}{do}{chrome.png}
	Insert text body here....
\end{Sbox}
```

The vast majority of text boxes take two additional arguments;

- the box type
- the sigil to use

The following box types are available:

- do
- copy
- run
- bonus

### Single line boxes: Sbox ###

This box is intended for a single line of text which will be aligned between the
text box tab on the left and the sigil on the right.

```latex
\begin{Sbox}{do}{chrome.png}
    Es etilius senium terferdium de Catudam.
\end{Sbox}
```

### Multi-line box: Mbox ###

This box contains no subdivisions and holds multi-line text bodies below the box
title tab and the sigil. 

```latex
\begin{Mbox}{do}{chrome.png}
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
    ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
    parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
    pellentesque eu, pretium quis, sem. 
\end{Mbox}
```	

### Single line + Multi-line box: SMbox + SMupper ###

This box comprises a SMupper box - containing a single line of text (see Sbox) within a SMbox which contains both the SMupper box and a multi-line text field (see Mbox).

Note:  The "\tcblower" command must be placed between the SMupper box and the multi-line text field within the SMbox.

```latex
\begin{SMbox}{copy}
    \begin{SMupper}{copy}{lamp.png}
        Es etilius senium terferdium de Catudam.
    \end{SMupper}

    \tcblower % This command divides the SMupper box from the multi-line text 
              % field below

    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
    ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
    parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
    pellentesque eu, pretium quis, sem. 

\end{SMbox}
```

### Multi-line + Multi-line box: MMbox ###

This box contains two multi-line fields divided by a horizontal partition. This
text box is built from two environments. The first `MMbox` simply defines the
base box colour and contains both the upper nested box environment `MMupper`
(containing the top multi-line text body) and the lower most multi-line text
body. 

A vital part of this text box is the `\tcblower` command to separate the upper
`MMupper` environment from the lower multi-line text body.

```latex
\begin{MMbox}{run}
    \begin{MMupper}{run}{chrome.png}
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
        dis parturient montes, nascetur ridiculus mus. Donec quam felis,
        ultricies nec, pellentesque eu, pretium quis, sem. 
    \end{MMupper}

    \tcblower % This command delineates the "upper" and "lower" spaces within
              % MMbox

    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
    ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
    parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
    pellentesque eu, pretium quis, sem. 
\end{MMbox}
```

### Three-part box -> Single line upper box and a split lower box : Threepartbox (TSupper + Splitbox) ###

This text box comprises of three custom environments: A `Threepartbox`
environment within which is nested a `TSupper` and a `Splitbox` environment. The
`TSupper` environment is designed to hold a single line of text (similar to
`Sbox`) and the `Splitbox` environment contains two multi-line fields aligned
side by side and written left to right. 

Note: The `TSupper` and the `Splitbox` environments are divided by a
`\tcblower` command.

Note: The Splitbox text fields and are also partitioned by a `\tcblower`
command.

```latex
\begin{Threepartbox}{run}
    \begin{TSupper}{run}{chrome.png}
        Es etilius senium terferdium de Catudam.
    \end{TSupper}

    \tcblower % This command separates the upper box from the lower split box

    \begin{Splitbox}{run}
        Es etilius senium terferdium de Catudam. (LEFT SIDE)
        \tcblower
        Es etilius senium terferdium de Catudam. (RIGHT SIDE)
    \end{Splitbox}
\end{Threepartbox}
```

### Three-part box -> Multi-line upper box and a split lower box : Threepartbox (TMupper + Splitbox) ###

This text box comprises of three custom environments: A `Threepartbox`
environment within which is nested a `TMupper` and a `Splitbox` environment. The
`TMupper` environment is designed to hold a multi-line text body (similar to
Mbox) and the `Splitbox` environment contains two multi-line fields aligned side
by side and written left to right. 

Note1: The `TMupper` and the `Splitbox` environments are divided by a
`\tcblower` command.

Note2: The Splitbox text fields and are also partitioned by a `\tcblower`
command.

```
\begin{Threepartbox}{run}
    \begin{TMupper}{run}{chrome.png}
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
        dis parturient montes, nascetur ridiculus mus. Donec quam felis,
        ultricies nec, pellentesque eu, pretium quis, sem. 
    \end{TMupper}

    \tcblower % This command separates the upper box from the lower split box

    \begin{Splitbox}{run}
        Es etilius senium terferdium de Catudam. (LEFT SIDE)
        \tcblower
        Es etilius senium terferdium de Catudam. (RIGHT SIDE)
    \end{Splitbox}
\end{Threepartbox}
```

### Half-page boxes : halfpagebox ###

To resize any box type to be half a page, simply place the custom box type
within the custom environment `halfpagebox`.

To have an image placed next to a half-page box, simply begin `halfpagebox`
immediately after the another.

Note: To place an image beside a text box, the `\begin{halfpagebox}` of the
second half-page box MUST be on the line below the `\end{halfpagebox}` of the
first half-page box.

```latex
\begin{halfpagebox}
    \begin{Mbox}{bonus}{chrome.png}
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
        dis parturient montes, nascetur ridiculus mus. Donec quam felis,
        ultricies nec, pellentesque eu, pretium quis, sem. 
    \end{Mbox}
\end{halfpagebox}
\begin{halfpagebox}
    \begin{center}
        \includegraphics[width=.8\textwidth,keepaspectratio]{penguin.png}
    \end{center}
\end{halfpagebox}
```

### Gatekeeper box: gatekeeperdefeated ###

This text box is a simple environment that supports a text field to be placed
underneath the gatekeeper defeated title.

```latex
\begin{gatekeeperdefeated}
    Ignatum es etilius senium terferdium de contemo Catudam.
\end{gatekeeperdefeated}
```
